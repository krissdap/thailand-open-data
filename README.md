## TAMBON_AMPHOE_CHANGWAT.csv
 - Extracted from Digital Government Development Agency "Government Open Data" https://data.go.th/DatasetDetail.aspx?id=c6d42e1b-3219-47e1-b6b7-dfe914f27910
 - Original data contains 7,768 rows plus one header row.
 - Original data contains many duplicates.
 - This file removed duplicates, and only takes 3 columns, TAMBON_T (ตำบล), AMPHOE_T (อำเภอ), CHANGWAT_T (จังหวัด)